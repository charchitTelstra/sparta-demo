package com.telstra.ordersystem.UserService.exception;

public class UserNotFoundException extends Exception{
	
	private String code;
	private String message;
	
	public static UserNotFoundException getInstance(String iErrorCode, String strMsg){
		return new UserNotFoundException(iErrorCode,strMsg);
	}
	
	public UserNotFoundException(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
