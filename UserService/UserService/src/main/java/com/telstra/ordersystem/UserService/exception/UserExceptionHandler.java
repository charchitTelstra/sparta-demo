package com.telstra.ordersystem.UserService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserExceptionHandler {
	
	@ExceptionHandler({UserNotFoundException.class})
	public ResponseEntity<ErrorResource>  handleUNFInternalException( UserNotFoundException exception) {


		ErrorResource exceptionResponse = new ErrorResource();
		exceptionResponse.setCode(exception.getCode());
		exceptionResponse.setMessage(exception.getMessage());
		System.out.println(exceptionResponse);
		return  new ResponseEntity<ErrorResource>(exceptionResponse, HttpStatus.UNPROCESSABLE_ENTITY);
	}
	
	@ExceptionHandler({Exception.class})
	public ResponseEntity<ErrorResource>  handleInternalException(Exception exception) {


		ErrorResource exceptionResponse = new ErrorResource();
		exceptionResponse.setMessage(exception.getMessage());
		return  new ResponseEntity<ErrorResource>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
