package com.telstra.ordersystem.UserService.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.telstra.ordersystem.UserService.exception.UserNotFoundException;
import com.telstra.ordersystem.UserService.model.User;
import com.telstra.ordersystem.UserService.userrepository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private Environment env;
	
	public Integer addUser(User user) {
		
		Integer userId = userRepository.saveAndFlush(user).getUserId();
		return userId;

	}
	public User getUser(Integer userId) throws UserNotFoundException {
		
		User user = userRepository.findOne(userId);
		if(user==null) {
//			System.out.println(user+"in service exception");
			throw UserNotFoundException.getInstance(env.getProperty("ERCODE.UserNotFound"), env.getProperty("ERMSG.UserNotFound"));
		}
		return user;

	}

}
