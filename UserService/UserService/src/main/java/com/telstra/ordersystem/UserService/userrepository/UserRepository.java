package com.telstra.ordersystem.UserService.userrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telstra.ordersystem.UserService.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
