package com.telstra.ordersystem.UserService.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(value="stackTrace")
public class ErrorResource extends Exception{
	
	@JsonProperty("Error Code")
	private String code;
	
	
	@JsonProperty("Message")
	private String message;


	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}


	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	@Override
	public String toString() {
		return "ErrorResource [code=" + code + ", message=" + message + "]";
	}

}
