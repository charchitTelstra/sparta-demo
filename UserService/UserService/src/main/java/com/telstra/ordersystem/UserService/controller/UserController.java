package com.telstra.ordersystem.UserService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.ordersystem.UserService.Service.UserService;
import com.telstra.ordersystem.UserService.exception.UserNotFoundException;
import com.telstra.ordersystem.UserService.model.User;

@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	@PostMapping(value="/addUser")
	public ResponseEntity<String> addUser(@RequestBody User user){
		Integer userId=userService.addUser(user);
		return new ResponseEntity<String>("User added with id : "+userId, HttpStatus.OK);
	}
	
	@GetMapping(value="/getUser")
	public ResponseEntity<User> getUserById(@RequestParam("userId") Integer userId) throws UserNotFoundException{
		User user=userService.getUser(userId);
		System.out.println(user+"in controller exception");
		
		return new ResponseEntity<User>(user, HttpStatus.FOUND);
	}

}
