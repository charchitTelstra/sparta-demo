package com.telstra.ordersystem.orderservice.OrderService.model;

import java.util.List;

public class OrderDetails {
	
	private Integer userId;
	private List<Integer> productList;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public List<Integer> getProductList() {
		return productList;
	}
	public void setProductList(List<Integer> productList) {
		this.productList = productList;
	}
	
}
