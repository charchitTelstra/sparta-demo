package com.telstra.ordersystem.orderservice.OrderService.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.ordersystem.orderservice.OrderService.OrderRepository.OrderRepository;
import com.telstra.ordersystem.orderservice.OrderService.model.Item;
import com.telstra.ordersystem.orderservice.OrderService.model.OrderDetails;
import com.telstra.ordersystem.orderservice.OrderService.model.Orders;
import com.telstra.ordersystem.orderservice.OrderService.model.User;

@Service
public class OrderService {

	@Autowired
	OrderRepository orderRepository;
	@Autowired
	RestTemplate restTemplate;

	@Value("${UserService.Url}")
	private String userServiceUrl;

	@Value("${ItemService.Url}")
	private String itemServiceUrl;

	public Orders placeOrder(OrderDetails orderDetails) throws InterruptedException, ExecutionException {
		CompletableFuture<User> user =  findUser(orderDetails.getUserId());
		CompletableFuture<List<Item>> itemList= getItems(orderDetails.getProductList());
		CompletableFuture.allOf(user,itemList).join();
		Orders orders = new Orders();
		orders.setUser(user.get());
		orders.setItem(itemList.get());
		orders.setOrderDate(new Date(Calendar.getInstance().getTimeInMillis()));

		return orderRepository.saveAndFlush(orders);
	}
	
	@Async
	public CompletableFuture<User> findUser(Integer userId){
		User user = restTemplate.getForObject(userServiceUrl + userId, User.class);
		System.out.println(Calendar.getInstance());
		return CompletableFuture.completedFuture(user);
		
	}
	@Async
	public CompletableFuture<List<Item>> getItems(List<Integer> itemIdList){
		
		List<Item> itemList = new ArrayList<>();
		Double amount = 0d;
		for (Integer itemId : itemIdList) {
			System.out.println("item"+Calendar.getInstance());
			Item item = restTemplate.getForObject(itemServiceUrl + itemId, Item.class);
			if (item != null) {
				itemList.add(item);
				amount += item.getPrice();
			}
		}
		return CompletableFuture.completedFuture(itemList);
		
	}

	public Orders getOrder(Integer id) {
		Orders order = orderRepository.findOne(id);
//		System.out.println(order);
		return order;
	}

}
