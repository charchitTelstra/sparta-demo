package com.telstra.ordersystem.orderservice.OrderService.exception;

import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

@ControllerAdvice
public class OrderExceptionHandler {
	
	

	
//	@ExceptionHandler({Exception.class})
//	public ResponseEntity<ErrorResource>  handleWLTPInternalException(Exception exception) {
//
//		System.out.println(exception);
//		ErrorResource exceptionResponse = new ErrorResource();
//		exceptionResponse.setMessage("Order Not placed");
//		return  new ResponseEntity<ErrorResource>(exceptionResponse, HttpStatus.UNPROCESSABLE_ENTITY);
//	}
	@ExceptionHandler({HttpClientErrorException.class})
	public ResponseEntity<ErrorResource>  HttpStatusCodeException(HttpClientErrorException exception) throws JSONException {
		ErrorResource exceptionResponse = new ErrorResource();
		System.out.println();
		JSONObject jsonObj = new JSONObject(exception.getResponseBodyAsString());
		exceptionResponse.setCode((String) jsonObj.get("Error Code"));
		exceptionResponse.setMessage((String) jsonObj.get("Message"));
		return  new ResponseEntity<ErrorResource>(exceptionResponse, HttpStatus.valueOf(exception.getRawStatusCode()));
	}
}
