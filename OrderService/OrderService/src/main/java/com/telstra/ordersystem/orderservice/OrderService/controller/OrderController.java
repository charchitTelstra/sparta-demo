package com.telstra.ordersystem.orderservice.OrderService.controller;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.ordersystem.orderservice.OrderService.Service.OrderService;
import com.telstra.ordersystem.orderservice.OrderService.model.OrderDetails;
import com.telstra.ordersystem.orderservice.OrderService.model.Orders;
import com.telstra.ordersystem.orderservice.OrderService.model.User;

@RestController
public class OrderController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	OrderService orderService;
	
	@PostMapping(value="/placeOrder")
	public ResponseEntity<Orders> placeOrder(@RequestBody OrderDetails orderDetails) throws InterruptedException, ExecutionException{
		return new ResponseEntity<Orders>(orderService.placeOrder(orderDetails),HttpStatus.OK);
		
//		return null;
	}
	
	@GetMapping(value="/getorder")
	public ResponseEntity<Orders> getOrder(@RequestParam("id") Integer id){
		LOGGER.info("In get order");
//		System.out.println("In Cont"+id);
		return new ResponseEntity<Orders>(orderService.getOrder(id), HttpStatus.OK);
	}
	

}
