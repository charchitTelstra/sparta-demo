package com.telstra.ordersystem.orderservice.OrderService.OrderRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telstra.ordersystem.orderservice.OrderService.model.Orders;


@Repository
public interface OrderRepository extends JpaRepository<Orders, Integer> {

}
