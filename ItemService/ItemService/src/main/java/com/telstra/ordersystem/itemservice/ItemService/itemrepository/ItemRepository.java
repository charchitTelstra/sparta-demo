package com.telstra.ordersystem.itemservice.ItemService.itemrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telstra.ordersystem.itemservice.ItemService.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

}
