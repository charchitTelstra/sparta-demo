package com.telstra.ordersystem.itemservice.ItemService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.ordersystem.itemservice.ItemService.Service.ItemService;
import com.telstra.ordersystem.itemservice.ItemService.exception.ItemNotFoundException;
import com.telstra.ordersystem.itemservice.ItemService.model.Item;

@RestController
public class ItemController {
	
	@Autowired
	ItemService itemService;
	
	@PostMapping(value="/addItem")
	public ResponseEntity<String> addUser(@RequestBody Item item){
		Integer itemId=itemService.addItem(item);
		
		
		return new ResponseEntity<String>("Item Added with id "+itemId, HttpStatus.OK);
	}
	
	@GetMapping(value="/getItem")
	public ResponseEntity<Item> getUserById(@RequestParam("itemId") Integer itemId) throws ItemNotFoundException{
		Item item=itemService.getItem(itemId);
		return new ResponseEntity<Item>(item, HttpStatus.FOUND);
	}

}
